<?php

/**
 * Hook to declare actions on clipboard items or when an item is dropped in a node
 * 
 * @return array
 * 		array(
 * 			'action' => array(
 * 				'title' => 'Title on clipboard select',
 * 				'callback' => 'function_callback_on_items',
 *  			'callback_node' => 'function_callback_on_node_drop',
 * 			),
 * 		);
 */
function hook_clipboard_action(){return array();}

/**
 * Hook to alter the clipboard block
 * 
 * @param string clipboardId
 * The clipboard block id
 * 
 */
function hook_clipboard_block_alter($clipboardId){}

/**
 * Hook to add custom item display
 * Because the return array is content type keyed, keep in mind your custom display can be overrided
 * 
 * @param array $vars
 * 		$vars['nid'] content id
 * 		$vars['type_node'] content type
 * 		$vars['index'] item index, you must specify this attribute in your <li> output tag (index="")
 * 		$vars['source'] item source
 * 		$vars['options'] boolean array of item option :
 * 			'edit',
 * 			'delete',
 * 
 * @return array :
 * 		'output' : the item <li> output where you must specify an index attribute (index="") in case of sortable clipboard
 * 		'weight' : a numeric value to decide witch theme override the other in case of 2 keys are the same (this allow you to bypass others output).
 * 		Default output has 0 weight so you have to provide at least weight 1 to override it.
 */
function hook_clipboard_alter_item_ouput($vars){return array();}

/**
 * Hook to alter the returned clipboard disabled list
 * Some module want to hidde their used clipboard from targeted clipboard list
 * 
 * @return array : an array of block id
 */
function hook_clipboard_get_disabled_target_alter(){return array();}