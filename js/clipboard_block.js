(function($){
	//Lock during operation
	var lock = false;

	/**
	 * Implementation of Drupal.behaviors for clipboard block
	 */
	Drupal.behaviors.clipboardBlock = {
		// Module js boot
		attach:function(context, settings) {
			Drupal.behaviors.clipboardBlock.init(context, settings);
		},

		// Clipboard init
		init:function(context, settings){
			// Unbind event to prevent double trigger
			$(".clipboard-node").unbind();
			$(".clipboard-click-img").unbind();
			$(".clipboard-contextual li").unbind();
			$("img.clipboard-delete").unbind();
			$("img.clipboard-refresh").unbind();

			// Branding overflow
			$("#branding").css('overflow', 'inherit');

			// Prevent z-index over
			$(".clipboard-contextual ul").css('z-index', '590');

			// Init copy to clipboard by click
		    var $region = $(".clipboard-node");
		    $region.mouseenter(function(){
		    	Drupal.behaviors.clipboardBlock.switchContextual($(this).find(".clipboard-contextual"), 'show');
		    });
		    $region.mouseleave(function(){
		    	Drupal.behaviors.clipboardBlock.switchContextual($(this).find(".clipboard-contextual"), 'hide');
		    });

			// Attach hover behavior to trigger and ul.contextual-links.
		    $(".clipboard-node").hover(
				function(){
					$(this).addClass('clipboard-contextual-region-active');
				},
				function(){
					$(this).removeClass('clipboard-contextual-region-active');
				}
			);

			$(".clipboard-click-img").click(function(){
			    var $list = $(this).parent().find(".clipboard-contextual-block-list");

				if($list.css("display") == "none")
					$list.show("fast");
				else
					$list.hide("fast");
			});

			$(".img-no-display").css("display", "none");
			$(".img-display").css("display", "inline");

			$(".clipboard-contextual li").click(function(){
				var clipboard = $("#" + $(this).attr("blockid"));
				Drupal.behaviors.clipboardBlock.writeItem($(clipboard).attr("id"), $(this).attr("clipboardnid"), $(this).attr("clipboardtype"), $(this).attr("clipboardsource"));
			});

			// Init draggable elements
			// Can't init sort and drag together so one or another
			if($(".clipboard-drop").not(".no-sortable").length == 0)
				$(".clipboard-draggable").draggable({
					revert:'invalid',
	//		    	helper:Drupal.behaviors.clipboardBlock.cloneHelper,
					cursor:'move',
			    });

			$(".clipboard-draggable-node").draggable({
				revert:'invalid',
		    	helper:Drupal.behaviors.clipboardBlock.cloneHelper,
				cursor:'move',
		    });

		    // Initialize drop zone
			$(".clipboard-drop").not(".no-droppable").droppable({
				accept:function(element){
			        if($(this).has(element).length > 0)
			        	return false;
			        else
			        	return element.attr("clipboardnid") !== undefined;
				},
				activeClass:"clipboard-drop-active",
				hoverClass:"clipboard-drop-active-hover",
				tolerance:"pointer",
				drop:function(event, ui){
					var clipboard = $(this);
					ui.draggable.parent().addClass('dropped');
					$(ui.item).addClass('dropped');

					if(clipboard.length)
						Drupal.behaviors.clipboardBlock.drop(event, ui, clipboard);
				}
			});
/*
			$(".clipboard-drop-zone").not(".no-droppable").droppable({
				accept:".clipboard-draggable",
				activeClass:"clipboard-drop-active",
				hoverClass:"clipboard-drop-active-hover",
				tolerance:"pointer",
				drop:function(event, ui){
					var clipboard = $(this).parents("form:first").find(".clipboard-drop").not(".no-droppable");
					ui.draggable.parent().addClass('dropped');

					if(clipboard.length)
						Drupal.behaviors.clipboardBlock.drop(event, ui, clipboard);
				}
			});
*/
			$(".clipboard-item-drop").not(".no-droppable").droppable({
				accept:".clipboard-draggable-item",
				activeClass:"clipboard-item-drop-active",
				hoverClass:"clipboard-drop-active-hover",
				tolerance:"pointer",
				drop:function(event, ui){
					var clipboard = $(this);

					if(clipboard.length)
						Drupal.behaviors.clipboardBlock.drop(event, ui, clipboard);
				}
			});

			$(".clipboard-drop").not(".no-sortable").sortable({
//		    	axis:'y',
				revert:'invalid',
				cursor:'move',
				placeholder: "placeholder",
				stop:function(){
			        if ($(this).hasClass('dropped')){
			            $(this).sortable('cancel');
			            $(this).removeClass('dropped');
			        }
				},
				update:function(event, ui){
					var clipboard = $(ui.item).closest(".clipboard-drop");

					if(clipboard.length && !$(this).hasClass('dropped'))
						Drupal.behaviors.clipboardBlock.sortItem($(clipboard).attr("id"), $(ui.item).attr("index"), $(ui.item).index());
				},
			});

			$("img.clipboard-delete").click(function(){
				if(confirm("Delete all the items from the clipboard ?")){
					Drupal.behaviors.clipboardBlock.truncateClipboard($(this).attr("clipboardid"));
				}
			});

			$("img.clipboard-refresh").click(function(){
				Drupal.behaviors.clipboardBlock.refreshClipboard($(this).attr("clipboardid"));
			});

			Drupal.behaviors.clipboardBlock.deleteItemClick();
		},

		switchContextual:function(contextual, operation){
	    	var $imgClipboard = $(contextual).find(".clipboard-click-img");
	    	var $imgDragClipboard = $(contextual).find(".clipboard-drag-img");
			var $list = $(contextual).find(".clipboard-contextual-block-list");
	
	    	if(operation == "show"){
	    		$imgClipboard.css("display", "inline-block");
	    		$imgDragClipboard.css("display", "inline-block");
	    	}else{
	    		$imgClipboard.css("display", "none");
	    		$imgDragClipboard.css("display", "none");
				$list.css("display", "none");
	    	}
		},

		// Draggable clone helper
		cloneHelper:function(){
			var nid = $(this).attr("clipboardnid");
			var type = $(this).attr("clipboardtype");
			var source = $(this).attr("clipboardsource");
			var title = ($(this).attr("clipboardtitle").length > 40 ? $(this).attr("clipboardtitle").substring(0, 40) + '...' : $(this).attr("clipboardtitle"));
			var item = $("<div clipboardnid=\"" + nid + "\" clipboardtype=\"" + type + "\" clipboardsource=\"" + source + "\">" + title + "</div>");

			item.css('display', 'inline-block');
//			item.css('background-color', '#FFCC66');
			item.css('background', 'url("' + Drupal.settings.clipboardBlock.modulePath + '/img/ui_bg_glass.png") 50% 50% repeat-x');		
			item.css('padding', '4px');
			item.css('overflow', 'hidden');
			item.css('white-space', 'nowrap');
			item.css('z-index', '800');

			return item;
		},

		// Drop on clipboard
		drop:function(event, ui, clipboard){
			Drupal.behaviors.clipboardBlock.writeItem($(clipboard).attr("id"), $(ui.helper).attr("clipboardnid"), $(ui.helper).attr("clipboardtype"), $(ui.helper).attr("clipboardsource"));
//	    	Drupal.behaviors.clipboardBlock.switchContextual($(".clipboard-contextual"), 'hide');
		},

		// Attach all the delete icons handler
		deleteItemClick:function(){
			// Remove click handler
			$("img.delete-icon").unbind();

			// Attach delete icon handler
			$("img.delete-icon").click(function(){
				if(!lock){
					lock = true;
					var clipboard = $(this).closest(".clipboard-drop");
					var li = $(this).closest("li");

					$(clipboard).closest(".clipboard").find(".ajaxLoad").show();
					$.ajax({
						url:Drupal.settings.clipboardBlock.baseUrl + '/?q=clipboard/delete/' + $(clipboard).attr("id") + '/' + $(li).attr("index"),
						success:function(data, textStatus, jqXHR){
							$(clipboard).find("li").each(function(index){
								$(this).attr("index", index);									
							});
							$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
							lock = false;
						},
						error:function(data, textStatus, jqXHR){
							alert("There was an error during the operation");
							$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
							lock = false;
						},
					});
					li.remove();
				}else
					alert("An operation is already running (1)");
			});
		},

		writeItem:function(clipboardId, itemId, itemType, itemSource){
			$("#" + clipboardId).closest(".clipboard").find(".ajaxLoad").show();

			if(!lock){
				lock = true;

				$.ajax({
					url:Drupal.settings.clipboardBlock.baseUrl + '/?q=clipboard/write/' + clipboardId + '/' + itemId + '/' + itemType + '/' + itemSource,
					dataType:"json",
					success:function(data, textStatus, jqXHR){
						$("#" + data.clipboardid).append(data.data);
						Drupal.behaviors.clipboardBlock.deleteItemClick();
						$("#" + data.clipboardid).closest(".clipboard").find(".ajaxLoad").hide();
						lock = false;
					},
					error:function(data, textStatus, jqXHR){
						alert("There was an error during the operation");
						$("#" + data.clipboardid).closest(".clipboard").find(".ajaxLoad").hide();
						lock = false;
					},
				});
			}else
				alert("An operation is already running (2)");
		},

		sortItem:function(clipboardId, indexBefore, indexAfter){
			if(!lock){
				lock = true;
				var clipboard = $("#" + clipboardId);

				$(clipboard).closest(".clipboard").find(".ajaxLoad").show();
				$.ajax({
					url:Drupal.settings.clipboardBlock.baseUrl + '/?q=clipboard/sort/' + $(clipboard).attr("id") + '/' + indexBefore + '/' + indexAfter,
					success:function(data, textStatus, jqXHR){
						if(data.item_index != -1){
							$(clipboard).find("li").each(function(index){
								$(this).attr("index", index);									
							});
							Drupal.behaviors.clipboardBlock.deleteItemClick();
							$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
						}
						lock = false;
					},
					error:function(data, textStatus, jqXHR){
						alert("There was an error during the operation");
						$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
						lock = false;
					},
				});
			}else
				alert("An operation is already running (3)");
		},

		truncateClipboard:function(clipboardId){
			if(!lock){
				lock = true;
				var clipboard = $("#" + clipboardId);

				$(clipboard).closest(".clipboard").find(".ajaxLoad").show();
				$.ajax({
					url:Drupal.settings.clipboardBlock.baseUrl + '/?q=clipboard/truncate/' + $(clipboard).attr("id"),
					success:function(data, textStatus, jqXHR){
						$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
						$(clipboard).children().remove();
						lock = false;
					},
					error:function(data, textStatus, jqXHR){
						alert("There was an error during the operation");
						$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
						lock = false;
					},
				});
			}else
				alert("An operation is already running (4)");
		},

		refreshClipboard:function(clipboardId){
			if(!lock){
				lock = true;
				var clipboard = $("#" + clipboardId);

				$(clipboard).closest(".clipboard").find(".ajaxLoad").show();
				$(clipboard).load(Drupal.settings.clipboardBlock.baseUrl + '/?q=clipboard/view/' + $(clipboard).attr("id") + " li", function(response, status, xhr){
					$(clipboard).closest(".clipboard").find(".ajaxLoad").hide();
					lock = false;

					Drupal.behaviors.clipboardBlock.init(null, null);

					if(status == "error")
						alert("There was an error during the operation");
				});
			}else
				alert("An operation is already running (5)");
		},
	}
})(jQuery);