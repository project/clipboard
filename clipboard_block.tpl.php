<?php
/**
 * Don't forget to add clipboard-drop class at your ol
 * All the clipboard-drop ol will be droppable and sortable
 * If you don't want one or another just add no-droppable or / and no-sortable class
 */

  print $toolBar.'<ul id="'.$clipboardId.'" class="clipboard-drop">';

  foreach($items as $rowItem)
    print $rowItem;

  print '</ul>';